# Notebooks pour la spécialité 1ère NSI

_Patrice Thibaud : professeur de NSI au lycée Marguerite de Flandre (59147 Gondecourt)_

# 📜 Contributions et licence

* La plupart des documents proposés sur ce site sont sous licence creative commons : __BY-NC-SA__.  
Ils ont été conçus soit par l'équipe pédagogique de NSI du [lycée Marguerite de Flandre](https://marguerite-de-flandre-gondecourt.enthdf.fr/) soit par [moi](mailto::patrice.thibaud@ac-lille.fr).

![licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

_Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre_  
Merci de __respecter les conditions de la__ [__licence__](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/)

* Pour les documents provenants de sources extérieures : ces sources sont sytématiquement citées


# Accés aux notebook via binder

[![Notebooks ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatth%2Fnotebooks_1ere_nsi/master)  



# Dépôt git associé 

Contient cours, TP ... :  https://framagit.org/patth/nsi_1ere

# Contact

Pour toute demande : 📧 patrice.thibaud@ac-lille.fr
