{
 "cells": [
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "---\n",
    "title: \"Chapitre 2 Représentations des entiers en machine\"\n",
    "subtitle : \"PARTIE 1 - Cours : Les bases de numération\"\n",
    "papersize: a4\n",
    "geometry: margin=1.5cm\n",
    "fontsize: 12pt\n",
    "lang: fr\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "L'ensemble des entiers naturels $\\mathbb{N}$ regroupe le 0 et les nombres positifs $\\{{0;1;2;3;4\\cdots}\\}$.\n",
    "\n",
    "La notation décimale que nous sommes habitués à utiliser depuis notre enfance n'est pas la seule façon de réprésenter ces nombres, elle est de plus inadaptée aux machines informatiques."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ecriture en base 10\n",
    "\n",
    "## Notation décimale\n",
    "\n",
    "N'importe quel sytème d'écriture d'un nombre utilise un jeu de symboles (on parle d'_alphabet_).  \n",
    "Le système de notation décimale est un système __positionnel__ utilisant un alphabet de dix symboles : les dix chiffres  $\\{{0;1;2;3;4;5;6;7;8;9}\\}$.\n",
    "\n",
    "La position d'un chiffre dans cette représentation indique son poids en puissance de 10, par exemple pour  le nombre  __quatre cent cinquante sept__ :  \n",
    "  $\\bf \\underbrace{4}_{\\text{centaines}}\\underbrace{5}_{\\text{dizaines}}\\underbrace{7}_{\\text{unités}} = 4\\times 100 + 5 \\times 10 + 7 \\times 1$ \n",
    "\n",
    "Autrement dit :   \n",
    "  $\\bf 457 = 4\\times {\\color{red}10^{2}} + 5 \\times {\\color{red}10^{1}} + 7 \\times {\\color{red}10^{0}}$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__📝Application__ : _décomposer 345 632 en base 10_ \n",
    "\n",
    "  $345632 = 3\\times 10^{5} + 4\\times 10^{4}+5\\times 10^{3}+6\\times 10^{2}+3\\times 10^{1}+2\\times 10^{0}$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Décomposition généralisée dans une base donnée\n",
    "\n",
    "La représentation décimale d'une nombre __N__ est la suite ordonnée de _n_ chiffres : $\\bf a_{n-1} a_{n-2}\\cdots a_{1} a_{0}$\n",
    "\n",
    "Comme on l'a vu on peut donc décomposer cette représentation ainsi :  \n",
    "\n",
    "$\\bf N = a_{n-1} \\times 10^{n-1} + a_{n-2} \\times10^{n-2}+ \\cdots +a_2 \\times10^2 +a_1 \\times10^1 +a_0 \\times10^0$\n",
    "\n",
    "Par exemple pour le nombre 253 :  \n",
    " $ a_{2} = 2, a_{1} = 5, a_{0} = 3  $ --> n = 3 chiffres sont nécessaires."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut généraliser cette décomposition à n'importe quelle base __B__ de numération :  $\\bf N=\\sum\\limits_{{i=0}}^{n-1}{a_i \\times B^i} $\n",
    "\n",
    "soit :  $\\boxed{\\bf{N =a_{n-1} B^{n-1} + a_{n-2} B^{n-2}+ \\cdots +a_2 B^2 +a_1 B^1 +a_0 B^0}}$\n",
    "\n",
    "La représentation associée sera notée : $\\bf{(a_{n-1}a_{n-2}\\cdots a_{1}a_{0})}_{B}$ \n",
    "\n",
    "__📐 Point mathématique :__  \n",
    "_Le signe $\\sum$ se lit \"sigma\". Il décrit une série de plusieurs additions._  \n",
    "_Par exemple_  $ N=\\sum\\limits_{{i=1}}^{4}{2n}$ _se lit \"somme pour __n__ allant de 1 à 4 de __2n__._  \n",
    " _Cela signifie que l'on fait prendre au nombre n toutes les valeurs entières entre 1 et 4 et qu'on fait la somme des termes 2n :_ \n",
    "\n",
    " $N = 2\\times 1 + 2\\times 2 +2\\times 3 +2\\times 4 = 14$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ecriture en base 2\n",
    "\n",
    "L'alphabet binaire comporte uniquement deux symboles __0__ et **1**.  \n",
    "Ce mode de représentation est utilisé par les systèmes électroniques les plus courants car il traduit simplement le passage ou non d'un courant, l'existence ou non d'une tension dans un circuit...\n",
    "\n",
    "Lorsqu'il s'agit de représenter un entier naturel, si on écrit 1011 une confusion peut exister.   \n",
    "On choisira donc de ne rien préciser si on utilise une représentation décimale sinon la base utilisée sera indiquée comme ceci $\\bf (1011)_{2}$\n",
    "\n",
    "_Remarque_ : Attention, de nombreuses notations coexistent comme $1011_{2}$ ou $\\overline{1011}_{2}$ ou encore $\\overline{1011}^{2}$.\n",
    "\n",
    "## Décomposition et passage vers la notation décimale\n",
    "\n",
    "On décompose cette représentation ainsi :  \n",
    "$\\bf N = a_{n-1} \\times 2^{n-1} + a_{n-2} \\times2^{n-2}+\\cdots +a_2 \\times2^2 +a_1 \\times2^1 +a_0 \\times2^0$ avec $a_{i} \\in  \\{0;1\\}$\n",
    "\n",
    "La position d'un chiffre dans cette représentation indique son poids en puissance de 2.  \n",
    "Le nombre binaire précédent utilise _n = 4_ symboles\n",
    "\n",
    "$\\bf (\\underbrace{1}_{\\text{huitaines}} \\underbrace{0}_{\\text{quatraines}} \\underbrace{1}_{\\text{deuzaines}} \\underbrace{1}_{\\text{unités}})_{2} = 1\\times {\\color{red}2^{3}} + 0\\times {\\color{red}2^{2}} + 1 \\times {\\color{red}2^{1}} + 1 \\times {\\color{red}2^{0}}$ \n",
    "\n",
    "Soit, si on donne la représentation en notation décimale : \n",
    "\n",
    "$\\bf (1011)_{2} = 1\\times 8 + 0\\times 4 + 1 \\times 2 + 1 \\times 1 =  8 + 0 + 2 +1 = 11$ \n",
    "\n",
    "On constate que pour représenter le nombre onze en notation décimale on utilise 2 symboles contre 4 en notation binaire.  \n",
    "\n",
    "_Remarques_ :   \n",
    "* Ces notations sont à longueur variables (peu de symboles pour un petit nombre, plus pour un plus grand)   \n",
    "* Plus la base à un alphabet conséquent, moins il faudra de symboles pour représenter un nombre.   \n",
    "\n",
    "Enfin, il peut être intéressant de connaitre quelques puissances courantes de 2 \n",
    "\n",
    "|$\\bf n$|0|1|2|3|4|5|6|7|8|  \n",
    "|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|  \n",
    "|$\\bf 2^{n}$|1|2|4|8|16|32|64|128|256|  \n",
    "\n",
    "## De la notation décimale à la notation binaire\n",
    "\n",
    "L'écriture d'un nombre décimal en binaire s'appuie sur sa décomposition en puissance de 2 (en choisissant à chaque fois la plus grande possible).\n",
    "\n",
    "$42 = 32 + 8 + 2 = 2^{5} + 2^{3} + 2^{1}$  \n",
    "\n",
    "Toutes les positions de 0 à n-1 (ici 5) doivent contenir un symbole ce qui donne :\n",
    "\n",
    "$42 = \\boxed1 \\times 2^{5} + \\boxed0 \\times 2^{4} + \\boxed1 \\times 2^{3} +\\boxed0 \\times 2^{2} +\\boxed1 \\times 2^{1} + \\boxed0 \\times 2^{0}$  \n",
    "\n",
    "soit $42=(101010)_{2}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__📌 Méthode :__\n",
    "<div style=\"display:inline-block; font-weight:bold; background-color:darksalmon\">\n",
    "On réalise des divisions euclidiennes successives par 2 jusqu'à obtenir un quotient nul.  <br>\n",
    "La représentation binaire est obtenue en lisant les restes du dernier vers le premier.\n",
    "</div>\n",
    "\n",
    "![divisions successives](./fig/divisions_successives.jpg)\n",
    "[^1]\n",
    "\n",
    "On retrouve bien le résultat précédent\n",
    "\n",
    "## Représentation en machine"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:inline-block; font-weight:bold; background-color:darksalmon\">\n",
    "> 📢 Un __bit__ (_binary digit_) correspond à l'unité d'information apportée par une variable binaire."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En mémoire machine la représentation d'un entier naturel sera donc stockée sur plusieurs __bits__ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:inline-block; font-weight:bold; background-color:darksalmon\">\n",
    "> 📢 L'assemblage de plusieurs symboles binaires constitue un __codet__ (l'équivalent d'un mot)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans nos langues les mots ont une longueur variable, en informatique pour une question d'efficacité cette longueur est de taille fixe multiple d'un octet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:inline-block; font-weight:bold; background-color:darksalmon\">\n",
    "> 📢 Un __octet__ est une séquence ordonnée de 8 bits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les mots utilisés pour coder les entiers sont couramment de taille __8 , 16, 32 ou 64__ bits.\n",
    "Ceci entraine limitation sur la taille des entiers naturels codés.   \n",
    "\n",
    "__Quels entiers peut-on coder avec 8 bits ?__   \n",
    "* L'entier naturel de valeur minimale est $(00000000)_{2}$ soit 0   \n",
    "* L'entier naturel de valeur maximale est $(11111111)_{2}$ soit 255   \n",
    "\n",
    "On peut généraliser cette démarche à n'importe quelle taille de mots."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:inline-block; font-weight:bold; background-color:darksalmon\">\n",
    "> 📢 __n__ bits permettent de représenter tous les entiers naturels compris entre **0** et $\\bf 2^{n} - 1$  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__📝Application__ :  \n",
    "Si on reprend l'exemple précédent,  le codage des entiers est effectué sur n = 8 bits.    \n",
    "La valeur minimale est bien 0, et la maximale : $2^{8}-1 =256-1 =255$\n",
    "\n",
    "__Il est donc important de préciser sur combien de bits sont codés les entiers manipulés.__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_\"Il y a 10 sortes de personnes : celles qui comprennent le binaire et celles qui ne le comprennent pas.\"_\n",
    "\n",
    "\n",
    "# Ecriture en base 16 (hexadécimale)\n",
    "\n",
    "## Intérêt\n",
    "\n",
    "Pour la base 16 les symboles utilisés sont les 10 chiffres et les 6 premières lettres de notre alphabet.  \n",
    "\n",
    "|alphabet hexadécimal|0|1|2|3|4|5|6|7|8|9|A|B|C|D|E|F|  \n",
    "|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|  \n",
    "|équivalence en décimal|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|  \n",
    "\n",
    "Cette notation simplifie la représentation des nombres car moins de symboles seront nécessaires pour représenter un nombre quen décimal par exemple.  \n",
    "Elle sera notamment utilisé pour représenter les données stockées en machine de manière plus lisible pour un humain.\n",
    "\n",
    "## Décomposition\n",
    "\n",
    "On décompose cette représentation ainsi :  \n",
    "\n",
    "$\\bf N = a_{n-1} \\times 16^{n-1} + a_{n-2} \\times16^{n-2}+ ..... +a_2 \\times16^2 +a_1 \\times16^1 +a_0 \\times16^0$\n",
    "\n",
    "Le principe de passage de l'hexadécimal vers le décimal est donc le même que celui du binaire vers le décimal si ce n'est que l'on remplacera les lettres utilisées par les valeurs décimales correspondantes.\n",
    "\n",
    "$\\bf (F2)_{16} = 15\\times {\\color{red}16^{1}} + 2\\times {\\color{red}16^{0}} = 240 + 2= 242$ \n",
    "\n",
    "__📝Application__ : _décomposer $\\bf (10C)_{16}$ en base 10_ \n",
    "\n",
    "  $\\bf (10C)_{16} = 1\\times 16^{2} + 0\\times 16^{1}+12\\times 16^{0} = 1\\times 256 + 0\\times 16 + 12\\times 1 = 268$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## De l'hexadécimal vers le binaire\n",
    "\n",
    "Tout groupe de 4 bits peut être représenté par un seul caractère hexadécimal.\n",
    "\n",
    "En effet :\n",
    "* la valeur minimale avec 4 bits en binaire est $(0000)_{2}$ soit   $\\bf (0)_{16}$  \n",
    "* la valeur maximale est $(1111)_{2}$ soit   $\\bf (F)_{16}$  \n",
    "\n",
    "__📌 Méthode :__  \n",
    "> Pour passer du binaire à l'hexadécimal, on regroupera donc les paquets par 4 \n",
    "\n",
    "__📝Application__ :  $\\bf (\\overbrace{1011}^{\\text{B}} \\overbrace{1110}^{\\text{E}})_{2}  = (BE)_{16}$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 💾 Utilisation des bases numériques en Python\n",
    "\n",
    "## Préfixes et notations\n",
    "\n",
    "En Python comme dans beaucoup d'autres langages on peut directement écrire les nombres en binaire en utilisant le préfixe __`0b`__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "0b101010"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En effet :  ${(101010)}_{2}=42$  \n",
    "\n",
    "Pour les nombres hexadécimaux : le préfixe utilisé est __`0x`__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "0x2A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Là aussi :  ${(2A)}_{16}=42$\n",
    "\n",
    "##  Les opérateurs de comparaison\n",
    "\n",
    "Nous souhaitons faire la comparaison du contenu de 2 représentations nommées X et Y grâce à la syntaxe suivante : `X operateur Y`\n",
    "    \n",
    "Voici, ci-dessous le lien entre les symboles mathématiques et les opérateurs python correspondants\n",
    "   \n",
    "|Symbole mathématique|Opérateur python|  \n",
    "|--------------------|----------------|  \n",
    "|$=$                 |`==`            |  \n",
    "|$\\neq$              |`!=`            |  \n",
    "|$>$                 |`>`             |  \n",
    "|$\\geq$              |`>=`            |  \n",
    "|$<$                 |`<`             |  \n",
    "|$\\leq$              |`<=`            |  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Par exemple l'expression  `12 > 13` sera évaluée comme la réponse à la question *\"Est-ce que le nombre entier 12 est supérieur au nombre entier 13 ?\"*  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "12 > 13"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'évaluation de telles expressions conduit nécessairement à deux réponses possibles Vrai ou Faux : en langage Python `True`  ou `False`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "14 >= 13"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ces deux valeurs particulières appartiennent à un nouveau type de données : les __booléens__ (_cette notion sera approfondie ultérieurement_)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut notamment vérifier l'égalité des représentations du nombre 42 en bases binaire et hexadécimale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "0b101010 == 0x2A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[^1]: Conversion décimal-Binaire https://zestedesavoir.com\n",
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
